# TC1004B.501 - Implementacion de internet de las cosas #

Reto: Sistema de monitoreo 

Sensores para medir:
* temperatura (valor actual y los valores históricos) - dato + gráfico
* humedad (valor actual) - gauge
* distanciamiento (valor actual) - dato + indicador color semaforo

Servidor receptor de datos y generador de visualizaciones al visitar un URL.

### Estructura de archivos ###

* arduino
    sensorgerardo
        evidencia.ino
    sensormatias
        evidencia.ino
    sensormichelle
        evidencia.ino
• data
    gerardo
        all.php
        database.php
        in.php
        last.php
    matias
        all.php
        database.php
        in.php
        last.php
    michelle
        all.php
        database.php
        in.php
        last.php
    index.html
    js
        chartGerardo.js
        chartMatias.js
        chartMichelle.js
        gaugeGerardo.js
        gaugeMatias.js
        gaugeMichelle.js
