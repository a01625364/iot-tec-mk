// Create Chart with no data
var ctxGerardo = document.getElementById('myChartGerardo').getContext('2d');
var myChartGerardo = new Chart(ctxGerardo, {
  type: 'line',
  data: {
    labels: [],     // Labels are empty
    datasets: [{
      label: 'Temp. (C)',
      data: [],     // Data is empty Adding it later, allows to see a pretty animation!
      fill: false,
      borderColor: 'rgba(255, 99, 132, 1)',   
      borderWidth: 1,
      lineTension: 0
    }]
  },
  options: {
    responsive: true,
    hoverMode: 'index',
    stacked: false,
    title: {
      display: false,
    },
    scales: {
      yAxes: [{
        ticks: {
            beginAtZero: true
        }
      }]
    }
  }
});

// Function to add new data to a chart
function addDataGerardo(chart, label, data) 
{
  chart.data.labels.push(label);
  chart.data.datasets.forEach((dataset) => 
  {
    dataset.data.push(data);
  });
  chart.update();
}

// Plot all the data at the database
$.ajax(
  '../data/gerardo/all.php',
  {
    success: function(data) {
      var jsonDataGerardo = JSON.parse(data);
      var temperaturaGerardo ;  // equivalent to temperaturaGerardo
      var humedadGerardo ;  // equivalent to humedad
      var proximidadGerardo ;  // equivalent to proximidad
      var sensorTimeGerardo ;  // converts timestamp to time (used as label)
      for(row in jsonDataGerardo){
        // Extract temperaturaGerardo
        temperaturaGerardo = jsonDataGerardo[row]['temperatura'];
        humedadGerardo = jsonDataGerardo[row]['humedad'];
        proximidadGerardo = jsonDataGerardo[row]['proximidad'];
        // Extract time from timestamp
        sensorTimeGerardo = new Date(jsonDataGerardo[row]['timestamp']).toLocaleTimeString();
        // Add data to chart
        addDataGerardo(myChartGerardo, sensorTimeGerardo, temperaturaGerardo);
      }
      gaugeGerardo.set(humedadGerardo); // set value of the gaugeGerardo to the last value of humedad
    },
    error: function() {
      console.log('There was some error performing the AJAX call!');
    }
   }
  );
  

  // Every 0.5s check for new data
  function fetchLastDataGerardo(){
  $.ajax(
    '../data/gerardo/last.php',
    {
      success: function(data) {
        var jsonDataGerardo = JSON.parse(data);
        var temperaturaGerardo = jsonDataGerardo[0]['temperatura']; 
        var humedadGerardo = jsonDataGerardo[0]['humedad']; 
        var proximidadGerardo = jsonDataGerardo[0]['proximidad']; 
        var sensorTimeGerardo = new Date(jsonDataGerardo[0]['timestamp']).toLocaleTimeString();   
        /* 
        Use the last time the sensor was updated, and compare that time with
        last record time. If different, update table.
  
        This technique is for demonstration purposes. A better way, should be 
        add another field at the database and update it when data was added to chart.
        */
        if(myChartGerardo.data.labels[myChartGerardo.data.labels.length - 1] === sensorTimeGerardo)
        {
          // Do nothing
          console.log('No new data');
        }
        else
        {
          // Add new record to chart
          addDataGerardo(myChartGerardo, sensorTimeGerardo, temperaturaGerardo, proximidadGerardo);
          gaugeGerardo.set(humedadGerardo); // set actual value
        }
  
      },
      error: function() {
        console.log('There was some error performing the AJAX call!');
      }
    }
  );
  }
  
setInterval(function(){ 
  fetchLastDataGerardo(); 
}, 500);