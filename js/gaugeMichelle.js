var optsMichelle = {
  angle: 0.04, // The span of the gauge arc
  lineWidth: 0.3, // The line thickness
  radiusScale: 1, // Relative radius
  pointer: {
    length: 0.6, // // Relative to gauge radius
    strokeWidth: 0.035, // The thickness
    color: '#000000' // Fill color
  },
  staticLabels: {
      font: "10px sans-serif",  // Specifies font
      labels: [20, 30, 40],  // Print labels at these values
      color: "#000000",  // Optional: Label text color
      fractionDigits: 0  // Optional: Numerical precision. 0=round off.
  },
  limitMax: false,     // If false, max value increases automatically if value > maxValue
  limitMin: false,     // If true, the min value of the gauge will be fixed
  colorStart: '#6FADCF',   // Colors
  colorStop: '#8FC0DA',    // just experiment with them
  strokeColor: '#E0E0E0',  // to see which ones work best for you
  generateGradient: true,
  highDpiSupport: true,     // High resolution support
  percentColors: [[20, "#a9d70b" ], [45, "#f9c802"], [67, "#ff0000"]]   // Make color react according to the position of the needle
  
};

var targetMichelle = document.getElementById('gaugeMichelle'); // your canvas element
var gaugeMichelle = new Gauge(targetMichelle).setOptions(optsMichelle); // create sexy gaugeMichelle!

gaugeMichelle.maxValue = 40; // set max gaugeMichelle value
gaugeMichelle.setMinValue(20);  // Prefer setter over gaugeMichelle.minValue = 0
gaugeMichelle.animationSpeed = 57; // set animation speed (32 is default value)

