// Create Chart with no data
var ctxMichelle = document.getElementById('myChartMichelle').getContext('2d');
var myChartMichelle = new Chart(ctxMichelle, {
  type: 'line',
  data: {
    labels: [],     // Labels are empty
    datasets: [{
      label: 'Temp. (C)',
      data: [],     // Data is empty Adding it later, allows to see a pretty animation!
      fill: false,
      borderColor: 'rgba(255, 99, 132, 1)',   
      borderWidth: 1,
      lineTension: 0
    }]
  },
  options: {
    responsive: true,
    hoverMode: 'index',
    stacked: false,
    title: {
      display: false,
    },
    scales: {
      yAxes: [{
        ticks: {
            beginAtZero: true
        }
    }]
    }
  }
});

// Function to add new data to a chart
function addDataMichelle(chart, label, data) 
{
  chart.data.labels.push(label);
  chart.data.datasets.forEach((dataset) => 
  {
    dataset.data.push(data);
  });
  chart.update();
}

// Plot all the data at the database
$.ajax(
  '../data/michelle/all.php',
  {
    success: function(data) {
      var jsonDataMichelle = JSON.parse(data);
      var temperaturaMichelle ;  // equivalent to temperaturaMichelle
      var humedadMichelle ;  // equivalent to humedad
      var proximidadMichelle ;  // equivalent to proximidad
      var sensorTimeMichelle ;  // converts timestamp to time (used as label)
      for(row in jsonDataMichelle){
        // Extract temperaturaMichelle
        temperaturaMichelle = jsonDataMichelle[row]['temperatura'];
        humedadMichelle = jsonDataMichelle[row]['humedad'];
        proximidadMichelle = jsonDataMichelle[row]['proximidad'];
        // Extract time from timestamp
        sensorTimeMichelle = new Date(jsonDataMichelle[row]['timestamp']).toLocaleTimeString();
        // Add data to chart
        addDataMichelle(myChartMichelle, sensorTimeMichelle, temperaturaMichelle);
      }
      gaugeMichelle.set(humedadMichelle); // set value of the gaugeMichelle to the last value of humedad
    },
    error: function() {
      console.log('There was some error performing the AJAX call!');
    }
   }
  );
  

  // Every 0.5s check for new data
  function fetchLastDataMichelle(){
    $.ajax(
      '../data/michelle/last.php',
      {
        success: function(data) {
          var jsonDataMichelle = JSON.parse(data);
          var temperaturaMichelle = jsonDataMichelle[0]['temperatura']; 
          var humedadMichelle = jsonDataMichelle[0]['humedad']; 
          var proximidadMichelle = jsonDataMichelle[0]['proximidad']; 
          var sensorTimeMichelle = new Date(jsonDataMichelle[0]['timestamp']).toLocaleTimeString();   
          /* 
          Use the last time the sensor was updated, and compare that time with
          last record time. If different, update table.
    
          This technique is for demonstration purposes. A better way, should be 
          add another field at the database and update it when data was added to chart.
          */
          if(myChartMichelle.data.labels[myChartMichelle.data.labels.length - 1] === sensorTimeMichelle)
          {
            // Do nothing
            console.log('No new data');
          }
          else
          {
            // Add new record to chart
            addDataMichelle(myChartMichelle, sensorTimeMichelle, temperaturaMichelle, proximidadMichelle);
            gaugeMichelle.set(humedadMichelle); // set actual value
          }
    
        },
        error: function() {
          console.log('There was some error performing the AJAX call!');
        }
      }
    );
  }
  
setInterval(function(){ 
  fetchLastDataMichelle();
}, 500);