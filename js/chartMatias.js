// Create Chart with no data
var ctxMatias = document.getElementById('myChartMatias').getContext('2d');
var myChartMatias = new Chart(ctxMatias, {
  type: 'line',
  data: {
    labels: [],     // Labels are empty
    datasets: [{
      label: 'Temp. (C)',
      data: [],     // Data is empty Adding it later, allows to see a pretty animation!
      fill: false,
      borderColor: 'rgba(255, 99, 132, 1)',   
      borderWidth: 1,
      lineTension: 0
    }]
  },
  options: {
    responsive: true,
    hoverMode: 'index',
    stacked: false,
    title: {
      display: false,
    },
    scales: {
      yAxes: [{
        ticks: {
            beginAtZero: true
        }
    }]
    }
  }
});

// Function to add new data to a chart
function addDataMatias(chart, label, data) 
{
  chart.data.labels.push(label);
  chart.data.datasets.forEach((dataset) => 
  {
    dataset.data.push(data);
  });
  chart.update();
}

// Plot all the data at the database
$.ajax(
  '../data/matias/all.php',
  {
    success: function(data) {
      var jsonDataMatias = JSON.parse(data);
      var temperaturaMatias ;  // equivalent to temperaturaMatias
      var humedadMatias ;  // equivalent to humedad
      var proximidadMatias ;  // equivalent to proximidad
      var sensorTimeMatias ;  // converts timestamp to time (used as label)
      for(row in jsonDataMatias){
        // Extract temperaturaMatias
        temperaturaMatias = jsonDataMatias[row]['temperatura'];
        humedadMatias = jsonDataMatias[row]['humedad'];
        proximidadMatias = jsonDataMatias[row]['proximidad'];
        // Extract time from timestamp
        sensorTimeMatias = new Date(jsonDataMatias[row]['timestamp']).toLocaleTimeString();
        // Add data to chart
        addDataMatias(myChartMatias, sensorTimeMatias, temperaturaMatias);
      }
      gaugeMatias.set(humedadMatias); // set value of the gaugeMatias to the last value of humedad
    },
    error: function() {
      console.log('There was some error performing the AJAX call!');
    }
   }
  );
  

  // Every 0.5s check for new data
  function fetchLastDataMatias(){
  $.ajax(
    '../data/matias/last.php',
    {
      success: function(data) {
        var jsonDataMatias = JSON.parse(data);
        var temperaturaMatias = jsonDataMatias[0]['temperatura']; 
        var humedadMatias = jsonDataMatias[0]['humedad']; 
        var proximidadMatias = jsonDataMatias[0]['proximidad']; 
        var sensorTimeMatias = new Date(jsonDataMatias[0]['timestamp']).toLocaleTimeString();   
        /* 
        Use the last time the sensor was updated, and compare that time with
        last record time. If different, update table.
  
        This technique is for demonstration purposes. A better way, should be 
        add another field at the database and update it when data was added to chart.
        */
        if(myChartMatias.data.labels[myChartMatias.data.labels.length - 1] === sensorTimeMatias)
        {
          // Do nothing
          console.log('No new data');
        }
        else
        {
          // Add new record to chart
          addDataMatias(myChartMatias, sensorTimeMatias, temperaturaMatias, proximidadMatias);
          gaugeMatias.set(humedadMatias); // set actual value
          
        }
  
      },
      error: function() {
        console.log('There was some error performing the AJAX call!');
      }
    }
  );
  }
  
setInterval(function(){ 
  fetchLastDataMatias(); 
}, 500);