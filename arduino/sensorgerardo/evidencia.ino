#include "DHT.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

const char* ssid = "Totalplay-2E9E_EXT";
const char* password = "2E9EB891n643hY9S";
const char* mqtt_server = "mqtt.eclipse.org";

String serverName = "http://iot-tec-mk.gearhostpreview.com/data/gerardo/in.php?";

#define GREEN_LED 15
#define YELLOW_LED 13
#define RED_LED 12
#define ECHO_PIN 5
#define TRIGGER_PIN 4
#define DHTPIN 0
#define DHTTYPE DHT11

WiFiClient espClient;
PubSubClient client(espClient);

bool michelleLed = false;
bool matiasLed = false;
bool gerardoLed = false;
String humedadG = "";
String temperaturaG = "";
float distanceG = 0;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  // put your setup code here, to run once:
  pinMode(GREEN_LED, OUTPUT);
  pinMode(YELLOW_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(TRIGGER_PIN, OUTPUT);

  dht.begin();

  Serial.begin(9600);

  WiFi.begin(ssid, password);
  Serial.println("Conectando...");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Conectado exitosamente");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  // put your main code here, to run repeatedly:
  static float lastHumedad;
  static float lastTemperatura;
  static float lastDistance;
  static bool lastBool;
  bool currentBool;
  float humedad = handleHumedad();
  float temperatura = handleTemperatura();
  
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  currentBool = calcularDistancia();

  if (lastBool != currentBool) {
    String temp = (String)temperatura;
    String hum = (String)humedad;
    String data = (String)currentBool;
    //Cambiar la palabra "gerardo" del publish a su nombre
    client.publish("TC1004B/gerardo/PRY-distance", data.c_str());
    sendToServer(temp, hum, data);
    delay(1000);
  }
  lastBool = currentBool;

  if (lastTemperatura != temperatura) {
    String temp = (String)temperatura;
    String hum = (String)humedad;
    String data = (String)currentBool;
    //Cambiar la palabra "gerardo" del publish a su nombre
    client.publish("TC1004B/gerardo/PRY-temperatura", temp.c_str());
    sendToServer(temp, hum, data);
  }
  lastTemperatura = temperatura;

  if (lastHumedad != humedad) {
    String temp = (String)temperatura;
    String hum = (String)humedad;
    String data = (String)currentBool;
    //Cambiar la palabra "gerardo" del publish a su nombre
    client.publish("TC1004B/gerardo/PRY-humedad", hum.c_str());
    sendToServer(temp, hum, data);
  }
  lastHumedad = humedad;

  usersStatus();
  
}

bool calcularDistancia() {
  long duration;
  float distance;
  digitalWrite(TRIGGER_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);
  duration = pulseIn(ECHO_PIN, HIGH);
  distance = (duration * 0.1715) / 1000;
  if (distance < 0.30) {
    return true;
  } else {
    return false;
  }
}

float handleHumedad() {
  float h = dht.readHumidity();
  return h;
}

float handleTemperatura() {
  float t = dht.readTemperature();
  return t;
}

void usersStatus() {
  if (((matiasLed || michelleLed) && (matiasLed || gerardoLed) && (gerardoLed || michelleLed)) && !((matiasLed == true) && (michelleLed == true) && (gerardoLed == true))) {
    digitalWrite(YELLOW_LED, HIGH);
    digitalWrite(RED_LED, LOW);
    digitalWrite(GREEN_LED, LOW);
  } else if ((matiasLed == true) && (michelleLed == true) && (gerardoLed == true)) {
    Serial.println("Entro aqui");
    digitalWrite(RED_LED, HIGH);
    digitalWrite(YELLOW_LED, LOW);
    digitalWrite(GREEN_LED, LOW);
  } else if ((matiasLed == true) || (michelleLed == true) || (gerardoLed == true)){
    digitalWrite(GREEN_LED, HIGH);
    digitalWrite(YELLOW_LED, LOW);
    digitalWrite(RED_LED, LOW);
  } else {
    digitalWrite(GREEN_LED, LOW);
  }
  delay(500);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Mensaje recibido [");
  Serial.print(topic);
  Serial.print("] ");
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if(strcmp(topic, "TC1004B/gerardo/PRY-distance") == 0) {
    char gerardo = (char)payload[0];
    if (gerardo == '0') {
      gerardoLed = false;
    } else  {
      gerardoLed = true;
    }
  }

  if(strcmp(topic, "TC1004B/michelle/PRY-distance") == 0) {
    char michelle = (char)payload[0];
    if (michelle == '0') {
      michelleLed = false;
    } else  {
      michelleLed = true;
    }
  }

  if(strcmp(topic, "TC1004B/matias/PRY-distance") == 0) {
    char matias = (char)payload[0];
    if (matias == '0') {
      matiasLed = false;
    } else  {
      matiasLed = true;
    }
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Intentando conexión MQTT...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) {
      Serial.println("Conectado");
      client.publish("TC1004B-con", "hello world");
      client.subscribe("TC1004B/gerardo/PRY-distance");
      client.subscribe("TC1004B/gerardo/PRY-temperatura");
      client.subscribe("TC1004B/gerardo/PRY-humedad");
      client.subscribe("TC1004B/michelle/PRY-distance");
      client.subscribe("TC1004B/matias/PRY-distance");
      
    } else {
      Serial.print("fallo en conexión, rc=");
      Serial.print(client.state());
      Serial.println(" Reintentar en 5 segundos...");
      delay(5000);
    }
  }
}

int sendToServer(String t, String h, String p) {
  HTTPClient http;
  String serverPath = serverName;
  serverPath += "humedad=";
  serverPath += h;
  serverPath += "&temperatura=";
  serverPath += t;
  serverPath += "&proximidad=";
  serverPath += p;
  Serial.println(serverPath);

  http.begin(serverPath);
  int httpResponseCode = http.GET();
  
  if (httpResponseCode > 0){
    Serial.print("HTTP Response Code: ");
    Serial.println(httpResponseCode);
    String payload = http.getString();
    Serial.println(payload);
  } else {
    Serial.print("Error Code: ");
    Serial.println(httpResponseCode);
  }
  http.end();
}
